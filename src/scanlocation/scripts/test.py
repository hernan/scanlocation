#!/usr/bin/env python3
#-*- coding:utf-8   -*-


import rospy
import numpy as np










if __name__ == '__main__':
    rospy.init_node('testpynode')
    pts = np.float32([
        [0, 0],
        [0,100-1],
        [50-1, 100-1],
        [50-1, 0]
    ]).reshape(-1, 1, 2)
    print(pts)
    rospy.spin()
    # print(obstacle.status_power())

# [
#      [[ 0.  0.]]
#
#      [[ 0. 99.]]
#
#      [[49. 99.]]
#
#      [[49.  0.]]
# ]
