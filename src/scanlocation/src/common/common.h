//
// Created by sukai on 22-9-26.
//

#ifndef CONTNAV_COMMON_H
#define CONTNAV_COMMON_H
#include <iostream>


//服务返回状态
struct ServiceResultType

{
    static std::string ResultTypeOK;
    static std::string ResultTypeERROR;
};


//重定位
struct ScanToPointToPngType

{
    static   std::string initialposeStatic;  //静态重定位
    static   std::string initialposeStaticthread;  //多线程静态重定位
    static   std::string initialposeDynamic;  //动态重定位
    static   std::string initiallocation;  //融合定位
    static   std::string initialposeStop;//停止重定位
};





#endif //CONTNAV_COMMON_H






