//
// Created by itcast on 7/22/20.
//
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;
/**
 *
 * 模板匹配
 * 1. 读取原图
 * 2. 读取模板
 * 3. 根据模板匹配算法 找到最优的位置
 *
 * rosrun  scanlocation test_node
 */
int main(int argc, char **argv){


    ros::init(argc, argv, "test_node");
    ros::NodeHandle node_handle_;


    Mat src = imread("/home/sukai/workspace/scanlocation/src/scanlocation/output/image/zhaonimei.jpg");
    Mat temp = imread("/home/sukai/workspace/scanlocation/src/scanlocation/output/image/mei.jpg");

    // 调用api
    Mat result;
    matchTemplate(src,temp,result,TM_CCOEFF);

    // 找到匹配结果中的最大值
    double minVal,maxVal;
    Point minLoc,maxLoc;
    minMaxLoc(result,&minVal,&maxVal,&minLoc,&maxLoc);

    cout<<"  最大值："<<maxVal<<endl;
    cout<<"最合适的位置："<<maxLoc<<endl;

    // 在图像中绘制位置  temp.size().width：宽度； temp.size().height：高度
    Rect rect1(maxLoc.x,maxLoc.y,temp.size().width,temp.size().height);
    rectangle(src,rect1,Scalar(0,0,255),2);//绘制


    imshow("src",src);
    waitKey();

    ros::spin();
    return 0;
}
/**
找最小值
平方差匹配法CV_TM_SQDIFF
归一化平方差匹配法CV_TM_SQDIFF_NORMED

找最大值
相关匹配法CV_TM_CCORR
归一化相关匹配法CV_TM_CCORR_NORMED
系数匹配法CV_TM_CCOEFF
化相关系数匹配法CV_TMCCOEFF_NORMED
*/



